## About

A simple bash script that generates a fake YouTube video URL.

## Requirements

`bash`

## Usage

``bash
./fakeyturlgen.sh
``

## Extras

The script doesn't actually check if the video ID is fake and doesn't already exist, just the sheer improbability of accidentally creating an existing one is so small that it's most definetly fake.
