#!/bin/bash

# Generating a video ID.
id="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9\-_' | fold -w ${1:-11} | head -n 1)"

echo "https://www.youtube.com/watch?v=$id"

exit 0
